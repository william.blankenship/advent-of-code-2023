use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

const WORDS: [&str; 20] = [
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "zero", "one", "two", "three", "four",
    "five", "six", "seven", "eight", "nine",
];

fn solve(words: &[&str]) -> u64 {
    let mut total: u64 = 0;
    let path = Path::new("input");
    let file = File::open(path).unwrap();
    let reader = io::BufReader::new(file);
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        // Find the word closest to the beginning of the line
        let first = words
            .iter()
            .enumerate()
            .map(|(i, word)| (i % 10, line.find(word)))
            .filter(|(_, index)| index.is_some())
            .min_by_key(|(_, index)| index.unwrap())
            .map(|(digit, _)| digit)
            .unwrap();
        // Find the word closest to the end of the line
        let last = words
            .iter()
            .enumerate()
            .map(|(i, word)| (i % 10, line.rfind(word)))
            .filter(|(_, index)| index.is_some())
            .max_by_key(|(_, index)| index.unwrap())
            .map(|(digit, _)| digit)
            .unwrap();
        // Combine the digits into a single string
        let number_str = first.to_string() + &last.to_string();
        // Convert the string into a number and add it to total
        total += number_str.parse::<u64>().unwrap();
    }
    total
}

fn main() {
    println!("part 1: {}", solve(&WORDS[..10]));
    println!("part 2: {}", solve(&WORDS));
}
