use once_cell::sync::Lazy;
use regex::Regex;
use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

struct Parser {
    // Parse a game into an ID and a set of rounds
    game: Regex,
    // Parse a round into a set of selections
    selections: Regex,
}

// Only create the Parser once
static PARSER: Lazy<Parser> = Lazy::new(|| Parser {
    game: Regex::new("Game ([0-9]+): (.*)").unwrap(),
    selections: Regex::new("(([0-9]+) (blue|red|green))").unwrap(),
});

#[derive(Debug)]
struct Cubes {
    red: u64,
    green: u64,
    blue: u64,
}

impl Cubes {
    fn new(red: u64, green: u64, blue: u64) -> Self {
        Self { red, green, blue }
    }
}

fn game(bag: &Cubes, serialized: &str) -> (u64, u64) {
    let game = PARSER.game.captures(serialized).unwrap();
    let id = game[1].parse::<u64>().unwrap();
    let mut total = id;
    let mut seen = Cubes::new(0, 0, 0);
    for round in game[2].split(';') {
        for selection in PARSER.selections.captures_iter(round) {
            let color = &selection[3];
            let count = selection[2].parse::<u64>().unwrap();
            match color {
                "red" => seen.red = cmp::max(seen.red, count),
                "green" => seen.green = cmp::max(seen.green, count),
                "blue" => seen.blue = cmp::max(seen.blue, count),
                _ => panic!("Invalid selection color: {}", color),
            };
        }
    }
    if seen.red > bag.red || seen.green > bag.green || seen.blue > bag.blue {
        total = 0;
    }
    let power = seen.red * seen.green * seen.blue;
    (total, power)
}

fn solve(bag: &Cubes) -> (u64, u64) {
    let path = Path::new("input");
    let file = File::open(path).unwrap();
    let reader = io::BufReader::new(file);
    let mut total = 0;
    let mut power = 0;
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        let result = game(bag, &line);
        total += result.0;
        power += result.1;
    }
    (total, power)
}

fn main() {
    println!("{:?}", solve(&Cubes::new(12, 13, 14)));
}
